Client:
- React / React Router
- Redux Toolkit
- TypeScript
- Socket.io
- CSS Modules
- Storybook
