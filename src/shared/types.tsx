import type { ButtonProps as AntButtonProps } from "antd/lib/button";

export type SizeType = AntButtonProps["size"];
