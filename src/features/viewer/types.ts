export interface ViewerModel {
  id: number;
  email: string;
  isActivated: boolean;
}

export interface AuthResponse {
  accessToken: string;
}
