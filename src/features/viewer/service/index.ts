import { login } from "./login";
import { register } from "./register";
import { refreshAuth } from "./refresh-auth";

export const viewerService = {
  login,
  register,
  refreshAuth,
};
